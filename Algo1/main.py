import time


class BookStore():
    def __init__(self, author, number_of_page, price):
        self.author = author
        self.number_of_page = number_of_page
        self.price = price

    def __str__(self):
        return "author: " + self.author + ", " + \
               "number_of_page: " + str(self.number_of_page) + ", " + \
               "price: " + str(self.price)


def print_list(store):
    for i in store:
        print(i)


def selection_sort_by_number_of_page(number_of_page_list):
    swaps = 0
    comparing_time = 0
    for i in range(len(number_of_page_list)):
        comparing_time = comparing_time + 1
        for j in range(i + 1, len(number_of_page_list)):
            comparing_time = comparing_time + 1
            if number_of_page_list[i].number_of_page > number_of_page_list[j].number_of_page:
                comparing_time = comparing_time + 1
                number_of_page_list[i], number_of_page_list[j] = number_of_page_list[j], number_of_page_list[i]
                swaps = swaps + 1
    print("Total swaps: " + str(swaps))
    print("Total compare: " + str(comparing_time))
    return number_of_page_list





def merge_sort(price_list):
    comparing_time_merge_sort_1 = 0
    swaps_merge_1 = 0

    if len(price_list) > 1:
        comparing_time_merge_sort_1 = comparing_time_merge_sort_1 + 1
        mid = len(price_list) // 2
        left_list = price_list[:mid]
        right_list = price_list[mid:]

        sorted_left_list = merge_sort(left_list)
        sorted_right_list = merge_sort(right_list)
        return merge(sorted_left_list, sorted_right_list)
    else:
        swaps_merge_1 = swaps_merge_1 + 1
        return price_list


def merge(left_list, right_list):
    comparing_time_merge_sort_2 = 0
    swaps_merge_2 = 0

    add_list = []
    i = 0
    j = 0

    while i < len(left_list) and j < len(right_list):
        comparing_time_merge_sort_2 = comparing_time_merge_sort_2 + 1
        if left_list[i].price < right_list[j].price:
            comparing_time_merge_sort_2 = comparing_time_merge_sort_2 + 1
            add_list.append(left_list[i])
            i += 1
        else:
            add_list.append(right_list[j])
            j += 1
            swaps_merge_2 = swaps_merge_2 + 1
    add_list.extend(left_list[i:])
    add_list.extend(right_list[j:])
    return add_list

    comparing_time = comparing_time_merge_sort_1 + comparing_time_merge_sort_2
    swaps = swaps_merge_1 + swaps_merge_2
    print("Merge copmare :" + str(comparing_time))
    print("Merge swaps :" + str(swaps))



if __name__ == "__main__":
    bookStoreList = []
    author = 0
    number_of_page = 1
    price = 2
    file = open('text.txt')
    for line in file:
        values = line.split(',')
        store = BookStore(values[author], int(values[number_of_page]), int(values[price]))
        bookStoreList.append(store)

    print("Selection:")
    start_time = time.clock()
    selection_sort_by_number_of_page(bookStoreList)
    elapsed_time = time.clock() - start_time
    print_list(bookStoreList)
    print("Timing: " + str(elapsed_time))

    print("\n\nMerge Sort:")
    start_time = time.clock()
    merge_sort(bookStoreList)

    print_list(merge_sort(bookStoreList))
    elapsed_time = time.clock() - start_time

    print("Timing: " + str(elapsed_time))
