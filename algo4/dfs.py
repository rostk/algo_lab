from collections import defaultdict
import operator


class Company:
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node_to_graph(self, value):
        self.nodes.add(value)


    def add_edge_to_graph(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.distances[(from_node, to_node)] = distance

def DFS (graph, param, weight):
    visited = {param: weight}

    path = {}
    nodes = set(graph.nodes)


    while nodes:
        max_node = None
        for node in nodes:
            if node in visited:
                if max_node is None:
                    max_node = node
                elif visited[node]>visited[max_node]:
                    max_node = node

        if max_node is None:
            break

        nodes.remove(max_node)
        node_weight = visited[max_node]


        for edge in graph.edges[max_node]:
            weight = node_weight + graph.distances[(max_node, edge)]
            if edge not in visited or weight>visited[edge]:
                visited[edge] = weight
                path[edge] = max_node

    return visited


if __name__ == '__main__':
    graph = Company()
    index_node = 0
    index_node2 = 0

    with open('career.in') as read_file:
        n = int(read_file.readline())
        stage_list = read_file.readline().split()
        weight = int(stage_list[0])

        for i in range(n):
            node_list_1 = read_file.readline().split()
            index_node2+=1
            for j in range(len(node_list_1) - 1):
                index_node += 1
                graph.add_node_to_graph(index_node)

                for i in range(j, j + 2):
                    index_node2 += 1
                    graph.add_edge_to_graph(index_node, index_node2, int(node_list_1[i]))
                    if (i==j+1):
                        index_node2-=1
            stage_list = node_list_1


    sum = DFS(graph, 1, weight)
    print(max(sum.items(), key=operator.itemgetter(1))[1])