infinity = 10000000000


def read_input():
    out_data = open('server.txt', 'r').read().split('\n')

    n = int(out_data[0].split()[0])
    m = int(out_data[0].split()[1])

    clients = list(map(int, out_data[1].split()))
    edges = [[int(i) for i in edge.split()] for edge in out_data[2:]]

    return n, m, clients, edges


def init_matrix(n, m, edges):
    matrix = []

    for i in range(n):
        initial_values = [infinity] * n
        matrix.append(initial_values)
        matrix[i][i] = 0

    for i in range(m):
        matrix[edges[i][0] - 1][edges[i][1] - 1] = edges[i][2]
        matrix[edges[i][1] - 1][edges[i][0] - 1] = edges[i][2]

    return matrix


def floyd_algo(n, matrix):
    new_matrix = matrix[0:n][0:n]

    for k in range(n):
        for i in range(n):
            for j in range(n):

                if new_matrix[i][k] < infinity and new_matrix[k][j] < infinity:
                    new_matrix[i][j] = min(new_matrix[i][j], new_matrix[i][k] + new_matrix[k][j])

    return new_matrix


def main():
    n, m, clients, edges = read_input()
    matrix = init_matrix(n, m, edges)
    subrtng = list(set(range(1, n + 1)) - set(clients))

    value =floyd_algo(n, matrix)
    result = infinity

    for i in subrtng:
        result = min([result, max([value[i - 1][j - 1] for j in clients])])

    print(result)


if __name__ == '__main__':
    main()


